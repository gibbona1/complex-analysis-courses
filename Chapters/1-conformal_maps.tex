\setcounter{section}{0}
\section{Chapter 1}
\subsection{Conformal mappings}
\begin{definition}
An arc $\gamma$ is called regular if $\gamma\in C^1, \ \gamma'(z)\neq 0 \ \forall z\in [a,b]$
\end{definition}
\begin{definition}
The tangent vector at $z_0$ is an equivalence class of arcs 
$\gamma:[a,b]\to \bC, \ \gamma(a)=z_0$ s.t. 
$\gamma_1\sim\gamma_2\iff
|\gamma_1(t)-\gamma_2(t)|=o(t), \ t\to 0$
\end{definition}
\begin{rem}
$f(t)=o(t), \ t\to 0$ means $\frac{|f(t)|}{|t|}\to 0$

$f(t)=O(t), \ t\to 0$ means $\frac{|f(t)|}{|t|}$ is bounded
\end{rem}
\begin{definition}
An oriented angle between $\gamma_1,\gamma_2$ at $0\in (a,b)$ is 
$$\measuredangle(\gamma_1,\gamma_2)
=\arg{\frac{\gamma_2'(0)}{\gamma_1'(0)}}$$
It is well defined for $\gamma_1'(0), \gamma_2'(0)\neq 0$
\end{definition}
\begin{definition}
$f$ is $\bR$ differentiable at $z_0:$ 
$f(z)=f(z_0)+Df_{z_0}(z-z_0)+o(|z-z_0|) \quad z\to z_0$
\end{definition}
\begin{definition}
$Df$ is invertible $\iff \exists$ an inverse, a linear map $\iff det\rmat{u_x & u_y \\ v_x & v_y}\neq 0$ where $f=u+iv$
\end{definition}
\begin{rem}
Let $\mathbf{v}=(v_1,\dots,v_n) \quad \mathbf{w}=(w_1,\dots,w_n)$
$\mathbf{v}=\mathbf{w}A$

Bases have the same orientation 
$\iff \ det A>0$

On $\bC\simeq \bR^2$, the standard $\bR$-basis $(1,i)$ standard orientation.
\end{rem}
\begin{definition}
Let $f:\Omega\to \Omega'$ a map between open sets in $\bC$

$f$ is conformal at $z_0\in \Omega$ if
\begin{enumerate}[(1)]
    \item $f$ is $\bR$-differentiable at $z_0$
    \item $Df_{z_0}$ is invertible
    \item $\measuredangle_0(f\circ \gamma_1, f\circ \gamma_2)
    =\measuredangle_0(\gamma_1,\gamma_2)$ \quad $\gamma_1(0)=\gamma_2(0)=z_0, \ 
    \gamma_1'(0),\gamma_2'(0)\neq 0$
    
    \imp $(f\circ \gamma_j)'(0)=Df_{z_0}(\gamma_j'(0)), \ j=1,2$
\end{enumerate}
\end{definition}
\begin{rem}
$Df_{z_0}$ invertible, $\gamma_j'(0)
\imp Df_{z_0}(\gamma_0'(0))\neq 0$
\end{rem}

\begin{definition}
Let $L:V\to V$ be a map between $\bC$-vector spaces. For $\lambda\in \bC, \ v\in V$

$L$ is $\bC$-linear if $L(\lambda v)=\lambda L(v)$

$L$ is $\bC$-antilinear if $L(\lambda v)=\bar\lambda L(v)$
\end{definition}


\begin{theorem}
$f$ is conformal at $z_0 \iff f$ is $\bC$-differentiable at $z_0, \ f'(z_0)\neq 0$

\begin{sol}

$\underline{\Leftarrow}$ 

Assume $f$ is $\bC$-differentiable $\imp f$ is $\bR$-differentiable

$f(z)=f(z_0)+f'(z_0)(z-z_0)+o(|z-z_0|) \quad z\to \_0$

$Df_{z_0}(\Delta z)=f'(z_0)\cdot \Delta z$

$\imp Df_{z_0}^{-1}(\Delta w)=\frac1{f'(z_0)}\Delta w \imp f$ is invertible

$L(\Delta z)=a\Delta z, \ a\neq 0\imp L^{-1}(\Delta w)=\frac1a \Delta w$

$L(z)=az$

Let $a=re^{i\theta}\quad L(z)=re^{i\theta}z$

Let $v_1, v_2 \neq 0\qquad v_1=\gamma_1', \ v_2=\gamma_2'$

$\arg{\frac{L(v_2)}{L(v_1)}}
=\arg{\frac{re^{i\theta}v_2}{re^{i\theta}v_1}}
=\arg{\frac{v_2}{v_1}}$ - $f$ is angle preserving

\imp $f$ is conformal at $z_0$

$\underline{\Rightarrow}$ 

Choose the standard basis $\{1,i\}$

$\arg{\frac{i}{1}}=\frac\pi2+2\pi\bZ$

$Df_{z_0}:\bR^2\to \bR^2 \ (\bR^2\simeq \bC) \qquad 
v_1=Df_{z_0}(1), \ v_2=Df_{z_0}(i)$

$f$ is angle preserving, \imp 
$\arg{\frac{v_1}{v_2}}=\frac\pi2+2\pi\bZ$

$\imp \frac{v_1}{v_2}=re^{i\frac\pi2}=ir\imp v_2=irv_1$

Let $v_3=Df_{z_0}(1+i)=Df_{z_0}(1)+Df_{z_0}(i)=v_1+v_2$

$=v_1+irv_1=(1+ir)v_1$

But $\theta=\arg{\frac{v_1}{v_2}}=arg(1+i)=\frac\pi4\imp r=1\imp v_2=iv_1$

Now let $\{w,iw\}$ be an $\bR-$basis

$v_1=Df_{z_0}(w), \ v_2=Df_{z_0}(iw)\imp Df_{z_0}(iw)=iDf_{z_0}(w)$ - $i$-linearity

\imp $i$-linearity and $\bR$-linearity\imp $\bC$-linearity

and $\bC$-linearity and $\bR$-differentiable (since f is conformal) implies $\bC$-differentiable
\end{sol}
\end{theorem}

\begin{rem}
Complex structure on a vector space $V$ is $J:V\to V\quad J^2=Id$

$A:V\to V$ is $\bR$-linear \quad $A(Jv)=JAv$

\imp $A$ is $\bC$-linear w.r.t. complex structures defined by $J$
\end{rem}